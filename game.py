import random
import pygame
from socket import *
from pygame.locals import *
import time

BLUE = (0, 0, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
YELLOW = (255, 255, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

CHUNK = 15
WIDTH = CHUNK * 80
HEIGHT = CHUNK * 50

SNAKE_SIZE = CHUNK
APPLE_SIZE = CHUNK

FRAMERATE = 30
SLEEP_TIME = 0.02
DELTA_TIME = 0.005
# sleep_time = 0.08


class Entity():
    def __init__(self, surface, size, color):
        self.surface = surface
        self.size = size
        self.block = pygame.Surface((self.size, self.size))
        self.block.fill(color)

        self.x = random.randint(0, 80) * CHUNK
        self.y = random.randint(0, 50) * CHUNK

    def update(self):
        return

    def draw(self):
        self.surface.blit(self.block, (self.x, self.y))


class Apple(Entity):

    def __init__(self, surface, poisonous=False) -> None:
        super().__init__(surface, APPLE_SIZE, YELLOW if poisonous else RED)
        self.is_poisonous = poisonous

    def get_rect(self):
        return pygame.Rect(self.x, self.y, self.size, self.size)

    def update_position(self):
        self.x = random.randint(0, 80) * CHUNK
        self.y = random.randint(0, 50) * CHUNK


class Snake(Entity):
    def __init__(self, surface) -> None:
        super().__init__(surface, SNAKE_SIZE, GREEN)
        self.start_up()

    def start_up(self):
        self.score = 0
        self.time_delay = 0.06
        self.length = 5
        self.x = [CHUNK*10]*self.length
        self.y = [CHUNK*10]*self.length
        self.direction = "down"

    def move_up(self):
        self.direction = "up"

    def move_down(self):
        self.direction = "down"

    def move_right(self):
        self.direction = "right"

    def move_left(self):
        self.direction = "left"

    def get_head_rect(self):
        return pygame.Rect(self.x[0], self.y[0], self.size, self.size)

    def get_body_rects(self):
        return [pygame.Rect(self.x[i], self.y[i], self.size, self.size) for i in range(1, self.length)]

    def try_to_eat(self, apple):
        if self.get_head_rect().colliderect(apple.get_rect()):
            self.grow()
            apple.update_position()

    def try_to_eat_multiple(self, apples):
        collision_index = self.get_head_rect().collidelist(
            [a.get_rect() for a in apples])
        if collision_index != -1:
            if apples[collision_index].is_poisonous:
                self.shrink()
            else:
                self.grow()
            apples[collision_index].update_position()

    def trying_to_eat_self(self, continue_playing=True):
        if self.get_head_rect().collidelist(self.get_body_rects()) != -1:
            print("GAME -- OVER")
            if continue_playing:
                self.start_up()

            return True
        return False

    def grow(self):
        self.score += 1
        self.length += 1
        self.x.append(self.x[-1])
        self.y.append(self.y[-1])
        if self.time_delay > 0:
            self.time_delay -= DELTA_TIME

        if self.time_delay < 0:
            self.time_delay = 0

    def shrink(self):
        self.score -= 1
        self.time_delay += DELTA_TIME
        self.length -= 1
        self.x.pop()
        self.y.pop()

        if self.length == 0:
            print("GAME -- OVER")
            self.start_up()

    def update(self):
        self._move()
        self.check_out_of_window()
        self.trying_to_eat_self()
        # time.sleep(self.time_delay)
        pygame.time.wait(int(self.time_delay * 1000))

    def _move(self):
        for i in range(self.length-1, 0, -1):
            self.x[i] = self.x[i-1]
            self.y[i] = self.y[i-1]

        if self.direction == "up":
            self.y[0] -= self.size
        elif self.direction == "down":
            self.y[0] += self.size
        elif self.direction == "right":
            self.x[0] += self.size
        elif self.direction == "left":
            self.x[0] -= self.size

    # def check_out_of_window(self):
    #     if self.x[0] < 0:
    #         self.x[0] = WIDTH
    #     elif self.x[0] > WIDTH:
    #         self.x[0] = 0
    #     if self.y[0] < 0:
    #         self.y[0] = HEIGHT
    #     elif self.y[0] > HEIGHT:
    #         self.y[0] = 0

    def check_out_of_window(self):
        if any([self.x[0] < 0, self.x[0] > WIDTH, self.y[0] < 0, self.y[0] > HEIGHT]):
            print("GAME -- OVER")
            self.start_up()

    def draw(self):
        for i in range(self.length):
            self.surface.blit(self.block, (self.x[i], self.y[i]))


class Game:
    def __init__(self) -> None:
        pygame.init()
        pygame.font.init()
        self.font = pygame.font.Font(None, 36)
        self.running = True
        self.surface = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption("Snake")

        self.snake = Snake(self.surface)
        self.apples = [Apple(self.surface) for i in range(
            5)] + [Apple(self.surface, poisonous=True) for i in range(2)]
        self.entities = [self.snake] + self.apples

        self.clock = pygame.time.Clock()

    def run(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_UP:
                        self.snake.move_up()
                    if event.key == K_DOWN:
                        self.snake.move_down()
                    if event.key == K_RIGHT:
                        self.snake.move_right()
                    if event.key == K_LEFT:
                        self.snake.move_left()
                if event.type == QUIT:
                    self.running = False

            self.surface.fill(BLACK)

            [entity.update() or entity.draw() for entity in self.entities]

            self.snake.try_to_eat_multiple(self.apples)

            score_text = self.font.render(
                f'Score: {self.snake.score}', True, WHITE)
            self.surface.blit(score_text, (10, 10))

            pygame.display.flip()

            # time.sleep(SLEEP_TIME)
            self.clock.tick(FRAMERATE)


if __name__ == "__main__":
    game = Game()
    game.run()
